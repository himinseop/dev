server {
    listen       *:80;
    server_name  localhost 127.0.0.1 api-external.stg.wemakeprice.com 210.97.195.197 192.168.200.177;
    include      blockip.conf;
    allow        all;

    access_log  /usr/local/nginx/logs/ext-api/access.log  main;
    error_log   /usr/local/nginx/logs/ext-api/error.log  notice;

    root  /opt/ext-api/Public;

    if ( $request_method !~ ^(GET|HEAD|POST|PUT|DELETE)$ ) { return 405; }

    location = /nginx_status {
        stub_status  on;
    }

    location ~ /ext-api|apc|memcache/ {
        include        mon_allow.conf;
        deny           all;
        access_log     off;
        include        fastcgi_params;
        fastcgi_pass   unix:/usr/local/php7/var/run/ext-api.sock;
        fastcgi_param  SCRIPT_FILENAME $request_filename;
        break;
    }

    location ^~ /visitor/ {
        include        mon_allow.conf;
        deny           all;
        access_log     off;
        include        fastcgi_params;
        fastcgi_pass   unix:/usr/local/php7/var/run/ext-api.sock;
        fastcgi_param  SCRIPT_FILENAME  $document_root/index.php;
        fastcgi_param  SCRIPT_NAME      index.php;
        fastcgi_param  APP_ENV          stg;
    }

    location / {
        # pass the PHP scripts to unix socket
        include        fastcgi_params;
        fastcgi_pass   unix:/usr/local/php7/var/run/ext-api.sock;
        fastcgi_param  SCRIPT_FILENAME  $document_root/index.php;
        fastcgi_param  SCRIPT_NAME      index.php;
        fastcgi_param  APP_ENV          stg;
        fastcgi_param  QUERY_STRING     _url=$uri&$args;
    }

    location = /favicon.ico { access_log off; expires 30d; break; }

    location ~* \.(css|js|swf|jpg|jpeg|gif|css|png|ico)$ {
        access_log  /usr/local/nginx/logs/ext-api/access_css.log  css_log;
        break;
    }

    # deny access to .htaccess files
    location ~* /\.ht { deny all; }
}
